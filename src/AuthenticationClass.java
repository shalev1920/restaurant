
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;


/*all this class is about Autentication   login /register 
 and connection to the database 
 */
public class AuthenticationClass {

    private PreparedStatement ps;
    private Connection conn;

    public void connect() {
        //here we trying to connect to the database ( sql server )         
        try {
            String connURL = "jdbc:sqlserver://localhost:1433;databaseName=ratatouille;integratedSecurity=true";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(connURL);
            System.out.println("connection complete ");
        } //if thats not going well there is Exception 
        catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Exception while trying to connect");
            ex.printStackTrace();
        }
    }

    //this function is to login into user who alresy  exists 
    public void login_func(String username, String password, String id, login_form form) {

        try {
            String connURL = "jdbc:sqlserver://localhost:1433;databaseName=ratatouille;integratedSecurity=true";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn;
            conn = DriverManager.getConnection(connURL);
            System.out.println("connection complete ");

            //iam calling here to procedure in my database 
            String sql = "exec sign_in ?,?,? ";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, id);

            ResultSet rs = ps.executeQuery();
             // in this while im  see if the register was succeeded by printing 1/0  
            // if succeeded 1 
            // if failed  0   
            int sqlres = 0;
            while (rs.next()) {
                sqlres = rs.getInt(1);
                System.out.println("result of login :" + sqlres);
            }

            if (sqlres == 1) {
                JOptionPane.showMessageDialog(null, " welcome");
                form.dispose();
                main_screen n = new main_screen();
                n.setVisible(true);
                n.id=id;
                
            } else {
                JOptionPane.showMessageDialog(null, " incorrrect username or password or id ha ha ha ");
            }
            //if thats not going well there is Exception 
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Exception while trying to connect");
            ex.printStackTrace();
        }

    }

    public void register_func(String username, String password, String id, String City, String address, String age, String firstname, String lastname, sign_up form) {

        if (username.trim().length() < 4 || username.trim().length() >= 50) {
            JOptionPane.showMessageDialog(null, "Username must be between 4 and 50 characters ");

        }else if (password.trim().length() !=8 ) {
            JOptionPane.showMessageDialog(null, "password must be 8  characters ");
        } else if (id.trim().length() != 9) {
            JOptionPane.showMessageDialog(null, "id must be 9 characters ");
        } else {

            try {
                ResultSet rs;
                connect();
                //here iam calling to the proc in the sql
                String sql = "exec register ?,?,?,?,?,?,?,?";
                ps = conn.prepareStatement(sql);

                ps.setString(1, username);
                ps.setString(2, password);
                ps.setString(3, id);
                ps.setString(4, City);
                ps.setString(5, address);
                ps.setString(6, age);
                ps.setString(7, firstname);
                ps.setString(8, lastname);

                rs = ps.executeQuery();
                // in this while im  see if the register was succeeded by printing 1/0  
                // if succeeded 1 
                // if failed  0   
                int sqlres = 0;
                while (rs.next()) {
                    sqlres = rs.getInt(1);
                    System.out.println("result of Register :" + sqlres);
                }
                if (sqlres == 1) {
                    JOptionPane.showMessageDialog(null, " welcome to our system");
                    form.dispose();
                    login_form lg = new login_form();
                    lg.setVisible(true);
                    lg.setLocationRelativeTo(null);

                } else {
                    JOptionPane.showMessageDialog(null, " there is alredy user with that id.");
                }
                //if thats not going well there is Exception 
            } catch (SQLException ex) {
                System.out.println("-> " + ex);
                ex.printStackTrace();
            }
        }
    }
}
