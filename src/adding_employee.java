
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 this class is for adding a employee to your system (database)
 */
/**
 *
 * @author shalev
 */
public class adding_employee {

    private PreparedStatement ps;
    private Connection conn;

    public void connect() {
        //here we trying to connect to the database ( sql server )         
        try {
            String connURL = "jdbc:sqlserver://localhost:1433;databaseName=ratatouille;integratedSecurity=true";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

            conn = DriverManager.getConnection(connURL);
            System.out.println("connection complete ");
        } //if the connection is failed so ill get exception in the catch 
        catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Exception while trying to connect");
            ex.printStackTrace();
        }
    }

    public void adding_employee(String employee_t, String firstname, String lastname, String id, String city, String address, String age, String phone, String payType, String salary, String note) {

        if (phone.trim().length() != 10) {
            JOptionPane.showMessageDialog(null, "phone number  must be 10  numbers .");
        } else if (id.trim().length() != 9) {
            JOptionPane.showMessageDialog(null, "id must be 9 numbers");
        } else {
            try {
                connect();
                String sql = "exec insert_employees ?,?,?,?,?,?,?,?,?,?,?,?,?,?";
                ps = conn.prepareStatement(sql);

                ps.setString(1, firstname);
                ps.setString(2, id);
                ps.setString(3, address);
                ps.setString(4, city);
                ps.setString(5, age);
                ps.setString(6, phone);
                ps.setString(7, lastname);
                ps.setInt(8, 0);
                ps.setInt(9, 0);
                ps.setString(10, salary);
                ps.setString(11, note);
                ps.setInt(12, 0);
                ps.setInt(13, 0);
                ps.setInt(14, 0);
                // here iam have a combo box with two options or check or cash 
                // so if he clicked cash so in the database cash will be in 1 
                // and the check will be 0 
                if (payType == "chek") {
                    ps.setInt(8, 1);
                    ps.setInt(9, 0);
                } else if (payType == "cash") {
                    ps.setInt(8, 0);
                    ps.setInt(9, 1);
                }
                // here iam have a combo box with three  options or chef or cleaner 
                // or waiter so if he clicked cleaner  so in the database cleaner will be in 1 
                // and the others  will be 0
                // and the same thing on the others 
                if (employee_t == "Chef") {
                    ps.setInt(12, 0);
                    ps.setInt(13, 0);
                    ps.setInt(14, 1);
                } else if (employee_t == "Cleaner") {
                    ps.setInt(12, 0);
                    ps.setInt(13, 1);
                    ps.setInt(14, 0);
                } else if (employee_t == "waiter") {
                    ps.setInt(12, 1);
                    ps.setInt(13, 0);
                    ps.setInt(14, 0);
                }
                JOptionPane.showMessageDialog(null, "a New Employee Added");
                ps.execute();
                //if the connection is failed so ill get exception in the catch 
            } catch (SQLException ex) {
                System.out.println("-> " + ex);
                ex.printStackTrace();
            }

        }

    }

}
