
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 this class is for adding a provider to your system (database)
 */
/**
 *
 * @author shalev
 */
public class adding_provider {

    private PreparedStatement ps;
    private Connection conn;

    public void connect() {

        //here we trying to connect to the database ( sql server )         
        try {
            String connURL = "jdbc:sqlserver://localhost:1433;databaseName=ratatouille;integratedSecurity=true";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

            conn = DriverManager.getConnection(connURL);
            System.out.println("connection complete ");
        } //if thats not going well there is Exception 
        catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Exception while trying to connect");
            ex.printStackTrace();
        }
    }

    public void adding_provider(String firstname, String lastname, String id, String company_name, String products, String phone, String pay_t, String note) {
        if (phone.trim().length() != 10) {
            JOptionPane.showMessageDialog(null, "phone number  must be 10  numbers .");
        } else if (id.trim().length() != 9) {
            JOptionPane.showMessageDialog(null, "id must be 9 numbers");
        } else {
        }
        try {
            connect();
            String sql = "exec insert_provider ?,?,?,?,?,?,?,?,?";
            ps = conn.prepareStatement(sql);
            ps.setString(1, firstname);
            ps.setString(2, lastname);
            ps.setString(3, id);
            ps.setString(4, company_name);
            ps.setString(5, products);
                  // here iam have a combo box with two options or check or cash 
            // so if he clicked cash so in the database cash will be in 1 
            // and the check will be 0 
            if (pay_t == ("chek")) {
                ps.setString(6, "1");
                ps.setString(7, "0");
            } else {
                ps.setString(6, "1");
                ps.setString(7, "0");
            }
            ps.setString(8, phone);
            ps.setString(9, note);

            ps.execute();
            JOptionPane.showMessageDialog(null, "a New Provider Added");
        } //if thats not going well there is Exception 
        catch (SQLException ex) {
            System.out.println("-> " + ex);
            ex.printStackTrace();
        }
    }
}
