/*
 this object employee  created for shwoing all my employees in the software 
 */

/**
 *
 * @author shalev
 */
class employee {

    public int ID;
    public String FirstName, LastName, Identity, City, Address, Phone, Age, Salary;

    public employee(int employeeID, String firstName, String lastName, String Identity, String City, String Address,
            String Phone, String Age, String Salary) {
        this.ID = employeeID;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Identity = Identity;
        this.City = City;
        this.Address = Address;
        this.Phone = Phone;
        this.Age = Age;
        this.Salary = Salary;
    }

    public String getEmployeeIdentity() {
        return Identity;
    }

    public int getEmployeeID() {
        return ID;
    }

    public String getPhone() {
        return Phone;
    }

    public String getSalary() {
        return Salary;
    }

    public String getAge() {
        return Age;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getCity() {
        return City;
    }

    public String getAddress() {
        return Address;
    }
}
