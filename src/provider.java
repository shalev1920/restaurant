/*
 this object provider created for shwoing all my providers in the software 
 */

/**
 *
 * @author shalev
 */
public class provider {

    public int ID;
    public String FirstName, LastName, Identity, company, products, Phone;

    public provider(int providerID, String firstName, String lastName, String Identity, String company, String products, String Phone) {
        this.ID = providerID;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Identity = Identity;
        this.company = company;
        this.products = products;
        this.Phone = Phone;

    }

    public int getproviderID() {
        return ID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getproviderIdentity() {
        return Identity;
    }

    public String getcompany() {
        return company;
    }

    public String getproducts() {
        return products;
    }

    public String getPhone() {
        return Phone;
    }

}
