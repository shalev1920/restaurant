
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class sqlConnection {

    public static void main(String [] args){
       //here we trying to connect to the database ( sql server ) 
        try{
            String connURL = "jdbc:sqlserver://localhost:1433;databaseName=ratatouille;integratedSecurity=true";
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn;
            conn = DriverManager.getConnection(connURL);
            System.out.println("connection complete ");
        } 
        //if the connection is failed so ill get exception in the catch 
        catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Exception while trying to connect");
            ex.printStackTrace();
        }
        
        
    }
}
